var should = require('should')
  , fs = require('fs')
  , remoteUrl = 'https://raw.githubusercontent.com/ColoWen/OpenText/master/BiometWF/Workflows/Change_Request/Change_Request_Template/forReport_powerview.head.html'
  , otToAngularSchemaForm = require('../src/ot-to-angular-schema-form');

describe('ot-to-angular-schema-form', function() {

  describe('Filesystem', function() {

    it('should create the same file as expected', function(done) {
      var expectedForm = require('./expected/filesystem/form.json');
      var expectedSchema = require('./expected/filesystem/schema.json');

      otToAngularSchemaForm
        .parse('./test/resources/test.html')
        .then(function(data) {
          expectedForm.should.be.eql(data.form);
          expectedSchema.should.be.eql(data.schema);
          done();
        })
        .catch(done)
    });
  });

  describe('Remote Url', function() {

    it('should create the same file as expected', function(done) {
      var expectedForm = JSON.parse(fs.readFileSync(__dirname + '/expected/remote/form.json', 'utf8'));
      var expectedSchema = JSON.parse(fs.readFileSync(__dirname + '/expected/remote/schema.json', 'utf8'));

      otToAngularSchemaForm
        .parse(remoteUrl)
        .then(function(data) {
          expectedForm.should.be.eql(data.form);
          expectedSchema.should.be.eql(data.schema);
          done();
        })
        .catch(done);
    });
  });
});
