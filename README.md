ot-to-angular-schema-form
===========================
# Update to version 0.0.3

[![Build Status](https://app.codeship.com/projects/e75e7380-f762-0134-35bd-0ae7ee10c8ce/status?branch=master)](https://app.codeship.com/projects/210700)

[Changelog](https://bitbucket.org/blue-harvest/ot-to-angular-schema-form/raw/9ed1bde9b246caff9de48c7f8690eee9eff2061c/changelog.txt)

## Background

Working with OpenText to create workflow is awesome but if you require to create your own frontend to render the forms is 
a mess because every time something changes in OpenText it requires to change your frontend code too and it's not maintainable.

##### ot-to-angular-schema-form has been created to transform OpenText HTML in one format that can be used to generate forms easily.

ot-to-angular-schema-form can be used in two different ways:

* Request an url.
* Request a file in your filesystem.

When you execute ot-to-angular-schema-form you will get two files:

* schema.json -> contains a valid json schema.
* form.json -> contains a valid form object to be used in angular-schema-form.

See Usage for more details.

## Usage

### Install:

#### Use NPM:

**As a CLI:**

```
npm install ot-to-angular-schema-form -g
```

**To use it inside a node application:**

```
npm install ot-to-angular-schema-form --save-dev
```

### Common usage

#### As a CLI:

When *ot-to-angular-schema-form* is executed as a command line it requires two arguments:

 - Path to the HTML file. This path can be a filesystem path or a remote url.
 - Physical path to one folder in your filesystem to store the form.json and the schema.json
 
```
ot-to-angular-schema-form path/to/your/file.html path/to/folder/to/store/form-and-schema
```

#### Inside a node application:

```javascript
var otToAngularSchemaForm = require('ot-to-angular-schema-form');

otToAngularSchemaForm
    .parse('path/to/your/file.html') // This path can be a file in your filesystem or a remote url
    .then(function (data) {
        console.log(data.form);
        console.log(data.schema);
    });
```

## License

ot-to-angular-schema-form is licensed under the ISC license.

