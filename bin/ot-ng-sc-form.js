#!/usr/bin/env node
'use strict';

// Provide a title to the process in `ps`
process.title = '@ot-to-angular-schema-form/cli';

var path = require('path');
var fs = require('fs');
var otToAngularSchemaForm = require('../src/ot-to-angular-schema-form');
var htmlPath = process.argv[2];

var folderToStoreFiles = path.resolve(process.cwd(), process.argv[3]);

otToAngularSchemaForm
  .parse(htmlPath)
  .then(function (data) {
    fs.writeFileSync(path.join(folderToStoreFiles, 'form.json'), JSON.stringify(data.form, null, 4));
    fs.writeFileSync(path.join(folderToStoreFiles, 'schema.json'), JSON.stringify(data.schema, null, 4));
  })
  .catch(function (error){
    console.log(error);
  })
