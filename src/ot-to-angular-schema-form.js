var path = require('path');
var fs = require('fs');
var Promise = require('q');
var cheerio = require('cheerio');
var Ajv = require('ajv');
var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
var request = require('request-promise-any');

function isRemoteUrl(url) {
  return url.indexOf('://') !== -1;
}

function getRemoteData(htmlPath, deferred) {
  request(htmlPath)
    .then(function(html) {
      deferred.resolve(getSchemaAndFormData(html));
    })
    .catch(function(error) {
      deferred.reject(error);
    });
}

function getFilesystemData(htmlPath, deferred) {
  fs.readFile(path.resolve(process.cwd(), htmlPath), 'utf-8', function(error, html) {
    if(!error) {
      deferred.resolve(getSchemaAndFormData(html));
    } else {
      deferred.reject(error);
    }
  });
}

function getSchemaAndFormData(html) {
  const $ = cheerio.load(html);
  var $inputs = $('input, select, textarea, button');
  var form = [
    {
      "key": "models",
      "add": "New",
      remove: "Delete",
      "style": {
        "add": "btn-success",
        remove: "btn-danger"
      },
      "items": []
    }
  ];
  var schema = {
    "properties": {
      "models": {
        "items": {
          "properties": {}
        },
        required: [],
        type: "array"
      }
    }, type: "object"
  };
  $inputs.each(function(index, input) {
    var inputName = input.attribs.name || input.attribs.id;
    var inputTag = input.name.toLowerCase();
    var inputTitle = input.attribs.title || inputName;
    var inputPlaceholder = input.attribs.placeholder || inputName + ' ' + inputType + ' ' + inputTag;
    var inputType = (input.attribs.type || '').toLowerCase();
    var formInput = {};
    var repeated;

    if (inputName) {
      repeated = schema.properties.models.required.indexOf(inputName) !== -1;
      if (!repeated) {
        switch (inputTag) {
          case 'input':
            if (inputType !== 'button') {
              schema.properties.models.items.properties[inputName] = {
                "type": "string"
              };
              if (inputType === 'hidden') {
                formInput.key = 'models[].' + inputName;
                formInput.type = 'hidden';
              } else if (inputType === 'text' || inputType === '') {
                formInput.key = 'models[].' + inputName;
                formInput.title = inputTitle;
                formInput.placeholder = inputPlaceholder;
              }
            }
            break;
          case 'select':
            schema.properties.models.items.properties[inputName] = {
              "type": "string",
              "enum": []
            };
            schema.properties.models.required.push(inputName);
            formInput = {
              type: 'select',
              title: inputTitle,
              titleMap: []
            };
            input.children.forEach(function(item) {
              var value;
              var content;
              if (item.name && item.name.toLowerCase() === 'option') {
                value = item.attribs.value;
                content = item.children[0].data;
                schema.properties.models.items.properties[inputName].enum.push(value);
                formInput.titleMap.push({value: value, name: content});
              }
            });
            break;
          default:
            schema.properties.models.items.properties[inputName] = {
              "type": "string"
            };
            schema.properties.models.required.push(inputName);
            formInput = {
              key: 'models[].' + inputName,
              type: 'textarea',
              title: inputTitle,
              placeholder: inputPlaceholder
            };
        }
      }
      if (Object.keys(formInput).length) {
        form[0].items.push(formInput);
      }
    }
  });
  form.push({
    type: "submit",
    title: "Save"
  });

  return {
    form: form,
    schema: schema
  };
}

module.exports = {
  parse: function(htmlFilePath) {
    var deferred = Promise.defer();

    if (isRemoteUrl(htmlFilePath)) {
      getRemoteData(htmlFilePath, deferred)
    } else {
      getFilesystemData(htmlFilePath, deferred);
    }

    return deferred.promise;
  }
};
